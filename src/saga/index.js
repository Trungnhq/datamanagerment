import { takeLatest, call, put, select} from 'redux-saga/effects'
import axios from "axios"
import {
  SAVE_ACCOUNT_INFO,
  SAVE_ACCOUNT_INFO_SUCCESS,
  SAVE_ACCOUNT_INFO_FAILURE,
} from "../redux/actions/type.js"

function* saveUserInfo(data){
  console.log(data)
  yield put({type: SAVE_ACCOUNT_INFO_SUCCESS, data: data.data, password: data.password, username: data.username, key: data.dataKey})
}

//root saga
export default function* rootSaga() {
  yield takeLatest(SAVE_ACCOUNT_INFO, saveUserInfo)
}