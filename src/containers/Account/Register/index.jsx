import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import * as firebase from 'firebase/app';
import RegisterForm from '../../../shared/components/login/RegisterForm';
import ethereumjsWallet from 'ethereumjs-wallet';
import Config from "../../../config/firebase.js"
import DataBase from "../../../config/DatabaseConfig"
import SweetAlert from 'react-bootstrap-sweetalert';
import IMG_LOAD from "shared/img/process.gif"
import { withTranslation } from 'react-i18next';
class Register extends Component {
  static propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func,
    }).isRequired,
  };
  constructor(props){
    super(props);
    this.state={
      error: "",
      keyData: "",
      alert: null
    }
    this.initWallet = this.initWallet.bind(this);
    this.addDatatoFireBase = this.addDatatoFireBase.bind(this);
    this.hideAlert = this.hideAlert.bind(this);
  }

  initWallet(){
    const account = ethereumjsWallet.generate();
    const sign_account = ethereumjsWallet.generate();
    const privateKey_sign = "0x" + (sign_account.getPrivateKey()).toString("hex");
    const privateKey = "0x" + (account.getPrivateKey()).toString("hex");
    const publicKey = "0x" + (account.getPublicKey()).toString("hex");
    const address = "0x" + (account.getAddress()).toString("hex");

    const data = {
      privateKey: privateKey,
      publicKey: publicKey,
      address: address,
      sign: privateKey_sign
    }
    this.setState({
      keyData: data
    })
  }
  registerFireBase = (user) => {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block" }}
          title={this.props.t("dataList.processing")}
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          showConfirm={false}
        >
          <div style={{ textAlign: "center" }}>
            <img
              alt="{name}"
              src={IMG_LOAD}
              style={{ height: "100px", width: "100px" }}
            />
          </div>
        </SweetAlert>
      )
    });
    const { history } = this.props;
    firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
      .then(() => {
        this.setState({
          alert: (
            <SweetAlert
              success
              style={{ display: "block" }}
              title={this.props.t("dataList.success")}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              showCancel={false}
              confirmBtnText={this.props.t("dataList.cancle")}
              closeOnClickOutside={false}
            >
                  </SweetAlert>
          )
        });
       this.addDatatoFireBase(user.email)
        
      })
      .catch((error) => {
        this.setState({ error: error.message });
      });
  };
  hideAlert(){
    this.setState({
      alert: null
    })
    this.props.history.push('/log_in');
  }
  async addDatatoFireBase(email){
var name   = email.substring(0, email.lastIndexOf("@"));
    await this.initWallet();
    const data = {
      email: email,
      publicKey: this.state.keyData.publicKey,
      privateKey: this.state.keyData.privateKey,
      address: this.state.keyData.address,
      sign: this.state.keyData.sign
    }
    var database = firebase.database()
    var TempRef = database.ref("account");
    var TempRefChild = TempRef.child(`${name}`);
    await TempRefChild.set(data);
  }

  render() {
    const { error } = this.state;
    return (
      <div className="account account--not-photo">
        <div className="account__wrapper">
          <div className="account__card">
            <div className="account__head">
              <h3 className="account__title">Data
                <span className="account__logo"> Managerment
                  <span className="account__logo-accent">System</span>
                </span>
              </h3>
              <h4 className="account__subhead subhead">Tạo tài khoản</h4>
            </div>
            <RegisterForm onSubmit={this.registerFireBase} errorMessage={error} />
            <div className="account__have-account">
              <p>Đã có tài khoản? <Link to="/log_in">Đăng nhập</Link></p>
            </div>
          </div>
        </div>
        {this.state.alert}
      </div>
    );
  }
}

export default withRouter(withTranslation('common')(Register));
