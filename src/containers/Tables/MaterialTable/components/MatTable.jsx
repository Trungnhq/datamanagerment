import React, { Component } from 'react';
import { Card, CardBody, Col } from 'reactstrap';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';
import MatTableHead from './MatTableHead';
import MatTableToolbar from './MatTableToolbar';
import Firebase from "firebase"
import accessControl from "config/accessControl"
import web3 from "config/web3"
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
import IMG_LOAD from "shared/img/process.gif"
import Dialog from '@material-ui/core/Dialog';
import renderDatePickerField from
  'shared/components/form/DatePicker';
import { Field, reduxForm } from 'redux-form';
import TimetableIcon from 'mdi-react/TimetableIcon';
import moment from 'moment';
import axios from 'axios'
import { withTranslation } from 'react-i18next';
const API = "http://127.0.0.1:3000/getDecryptData"
let counter = 0;
const EthereumjsTx = require('ethereumjs-tx').Transaction;
function createData(id, value, time) {
  counter += 1;
  return {
    id: counter, id, value, time
  };
}

function getSorting(order, orderBy) {
  if (order === 'desc') {
    return (a, b) => {
      if (a[orderBy] < b[orderBy]) {
        return -1;
      }
      if (a[orderBy] > b[orderBy]) {
        return 1;
      }
      return 0;
    };
  }
  return (a, b) => {
    if (a[orderBy] > b[orderBy]) {
      return -1;
    }
    if (a[orderBy] < b[orderBy]) {
      return 1;
    }
    return 0;
  };
}

class MatTable extends Component {
  constructor(props) {
    super(props)
    this.state = {
      order: 'asc',
      orderBy: 'calories',
      selected: new Map([]),
      data: [],
      page: 0,
      rowsPerPage: 5,
      alert: null,
      openDatePicker: false
    };
    this.getUsers = this.getUsers.bind(this);
    this.filterList = this.filterList.bind(this);
    this.handleRequestDevice = this.handleRequestDevice.bind(this);
    this.handleGetData = this.handleGetData.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.listenAllEventConfirmSuccess = this.listenAllEventConfirmSuccess.bind(this)
  }
  async componentDidMount() {
    await this.listenAllEventConfirmSuccess()
    await this.getUsers();
  }
  async componentWillReceiveProps(preProps){
    if(preProps.userInfo !== this.props.userInfo){
      await this.getUsers();
    }
  }
  async handleSubmit(deviceID, email, owner) {
    await this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block" }}
          title={this.props.t("dataList.processing")}
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          showConfirm={false}
        >
          <div style={{ textAlign: "center" }}>
            <img
              alt="{name}"
              src={IMG_LOAD}
              style={{ height: "100px", width: "100px" }}
            />
          </div>
        </SweetAlert>
      )
    });
    let data = {
      deviceID: deviceID,
      email: email,
      date: this.state.dateGetData,
      owner: owner
    }
    axios({
      method: 'post',
      url: API,
      data: data
    })
      .then(res => {
        const data = res.data.data;
        console.log("Data", data)
        this.setState({
          alert: (
            <SweetAlert
              success
              style={{ display: "block" }}
              title={this.props.t("dataList.success")}
              onConfirm={() => this.hideAlert()}
              onCancel={() => this.hideAlert()}
              showConfirm={true}
            >
              <div style={{ textAlign: "center" }}>
          <p>{this.props.t("dataList.decrypt")}</p>
                <p>{this.props.t("dataList.temp")}: {data.substr(0, 2)}</p>
                <p>{this.props.t("dataList.humid")}: {data.substr(2, data.length)}</p>
              </div>
            </SweetAlert>
          )
        });
      })
      .catch(err => {
        console.log(err)
      })
  }
  listenAllEventConfirmSuccess() {
    const address = this.props.userInfo.key.address;
    accessControl.getPastEvents('ConfirmAccessDataSuccess', {
      filter: { requester: address },
      fromBlock: 0,
      toBlock: 'latest'
    }, (error, events) => {
      console.log("Check event", events);
      let result = [];
      for (let i = 0; i < events.length; i++) {
        console.log(events[i])
        if (events[i]) {
          result.push({
            requester: events[i].returnValues.requester,
            time: events[i].returnValues.time,
            deviceId: events[i].returnValues.deviceId,
            emailRequester: events[i].returnValues.emailRequester,
            owner: events[i].returnValues.owner
          })
        }
      }
      this.setState({ successList: result })
    })
      .then((events) => {
        // console.log(events) // same results as the optional callback above
      });
  }
  async handleRequestDevice(owner, deviceID, publicKey, email, emailOwner) {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block" }}
          title={this.props.t("dataList.processing")}
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          showConfirm={false}
        >
          <div style={{ textAlign: "center" }}>
            <img
              alt="{name}"
              src={IMG_LOAD}
              style={{ height: "100px", width: "100px" }}
            />
          </div>
        </SweetAlert>
      )
    });
    const email_bytes = await web3.utils.fromAscii(email)
    const dataRequest = accessControl.methods.requestData(
      owner,
      emailOwner,
      deviceID,
      publicKey,
      email_bytes
    ).encodeABI();
    let nonce = await web3.eth.getTransactionCount(this.props.userInfo.key.address);
    const txParams = {
      from: this.props.userInfo.key.address,
      nonce: nonce,
      gasPrice: web3.utils.toHex(web3.utils.toWei('50', 'gwei')),
      gasLimit: 4000000,
      to: "0x739a933e6eb8ba6824767ae691289b8ed3348d46",
      data: dataRequest,
    }
    const privateKey = this.props.userInfo.key.privateKey;
    let tx = new EthereumjsTx(txParams, { chain: 'ropsten', hardfork: 'petersburg' });
    let sign = await Buffer.from(privateKey.slice(2), 'hex');
    tx.sign(sign);
    const rawTx = '0x' + tx.serialize().toString('hex');
    web3.eth.sendSignedTransaction(rawTx,
      (error, result) => {
        if (error) {
          console.log(error);
          this.setState({
            alert: (
              <SweetAlert
                error
                style={{ display: "block" }}
                title={this.props.t("dataList.failed")}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
              >
               {this.props.t("dataList.success")}
              </SweetAlert>
            )
          });
        }
        else {
          console.log("Result", result);
          this.setState({
            alert: (
              <SweetAlert
                success
                style={{ display: "block" }}
                title={this.props.t("dataList.success")}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                showCancel={false}
                confirmBtnText={this.props.t("dataList.cancle")}
                closeOnClickOutside={false}
              >
                {this.props.t("dataList.success")}
                </SweetAlert>
            )
          });
        }
      })
  }
  hideAlert() {
    this.setState({
      alert: null
    })
  }
  async filterList() {
    const temp = this.state.ownerList;
    let list = [];
    for (let i = 0; i < temp.length; i++) {
      var listdevice = temp[i].deviceList;
      for (let j = 0; j < listdevice.length; j++) {
        var check = false;
        for (var z = 0; z < this.state.successList.length; z++) {
          console.log("Check 1", this.state.successList[z].requester);
          console.log("Check 2", this.props.userInfo.key.address)
          if (this.state.successList[z].deviceId === listdevice[j][0] && this.state.successList[z].requester.toLowerCase() === this.props.userInfo.key.address) {
            check = true;
          }
        }
        if (check === false) {
          await list.push(
            {
              name: web3.utils.toUtf8(listdevice[j][1]),
              deviceID: listdevice[j][0],
              status: this.props.t('dataList.waiting'),
              actions: (
                <div>
                  <Button color="info" size="sm"
                    onClick={() => this.handleRequestDevice(temp[i].address, listdevice[j][0], this.props.userInfo.key.publicKey, this.props.userInfo.username, listdevice[j][1])}
                  >
                    {this.props.t('dataList.request')}
                </Button>
                </div>
              )
            }
          )
        } else {
          let name = await web3.utils.toUtf8(listdevice[j][1])
          let iddevice = await listdevice[j][0]
          await list.push(
            {
              name: web3.utils.toUtf8(listdevice[j][1]),
              deviceID: listdevice[j][0],
              status: this.props.t('dataList.approved'),
              actions: (
                <div>
                  <Button
                    color="success"
                    size="sm"
                    //handleBanAccess={() => this.handleBanAccess()}
                    onClick={() => this.handleGetData(iddevice, this.props.userInfo.username, name)} >{this.props.t('dataList.getData')}</Button>
                </div>
              )
            }
          )
        }

      }

    }
    this.setState({
      data: list
    })
  }
  handleChange = evt => {
    console.log(typeof (evt))
    var date = moment(evt).format("DD");
    this.setState({
      dateGetData: date
    })
  }
  handleGetData(deviceID, email, owner) {
    console.log("owner", owner)
    console.log(deviceID)
    console.log(email)
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block" }}
          title="Vui lòng chọn thời gian"
          onConfirm={() => this.handleSubmit(deviceID, email, owner)}
          onCancel={() => this.hideAlert()}
          showConfirm={true}
          confirmBtnText="Xác nhận"
        >
          <form className="form"  >
            <div className="form__form-group">
              <span className="form__form-group-label">Date and Time Picker</span>
              <div className="form__form-group-field">
                <Field
                  name="date_time"
                  component={renderDatePickerField}
                  onChange={this.handleChange}
                />
                <div className="form__form-group-icon">
                  <TimetableIcon />
                </div>
              </div>
            </div>
            <div style={{ height: "200px" }}>

            </div>
          </form>

        </SweetAlert>
      )
    });
  }
  getUsers = async () => {
    const ownerList = await accessControl.methods.getUsers().call();
    this.setState({
      ownerList: [],
    })
    await ownerList.map(async (address) => {
      const deviceList = await accessControl.methods.getUserDevices(address).call();
      if(this.props.userInfo.key.address){
        if ((address.toString()).toLowerCase() !== this.props.userInfo.key.address.toLowerCase()) {
          await this.setState(prev => ({
            ownerList: [...prev.ownerList, { address, deviceList }]
          }))
        }
      }
      this.filterList();
    })

  }
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = 'desc';
    const { orderBy: stateOrderBy, order: stateOrder } = this.state;

    if (stateOrderBy === property && stateOrder === 'desc') { order = 'asc'; }

    this.setState({ order, orderBy });
  };

  handleSelectAllClick = (event, checked) => {
    if (checked) {
      const { data } = this.state;
      const newSelected = new Map();
      data.map(n => newSelected.set(n.id, true));
      this.setState({ selected: newSelected });
      return;
    }
    this.setState({ selected: new Map([]) });
  };

  handleClick = (event, id) => {
    const { selected } = this.state;
    const newSelected = new Map(selected);
    const value = newSelected.get(id);
    let isActive = true;
    if (value) {
      isActive = false;
    }
    newSelected.set(id, isActive);
    this.setState({ selected: newSelected });
  };

  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  handleChangeRowsPerPage = (event) => {
    this.setState({ rowsPerPage: event.target.value });
  };

  handleDeleteSelected = () => {
    const { data } = this.state;
    let copyData = [...data];
    const { selected } = this.state;

    for (let i = 0; i < [...selected].filter(el => el[1]).length; i += 1) {
      copyData = copyData.filter(obj => obj.id !== selected[i]);
    }

    this.setState({ data: copyData, selected: new Map([]) });
  };

  isSelected = (id) => {
    const { selected } = this.state;
    return !!selected.get(id);
  };

  render() {
    const {
      data, order, orderBy, selected, rowsPerPage, page,
    } = this.state;
    const {t} = this.props;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, data.length - (page * rowsPerPage));

    return (
      <Col md={12} lg={12}>
        <Card>
          <CardBody>
            <div className="card__title">
              <h5 className="bold-text">{this.props.name}</h5>
            </div>
            <MatTableToolbar
              numSelected={[...selected].filter(el => el[1]).length}
              handleDeleteSelected={this.handleDeleteSelected}
              onRequestSort={this.handleRequestSort}
            />
            <div className="material-table__wrap">
              <Table className="material-table">
                <MatTableHead
                  numSelected={[...selected].filter(el => el[1]).length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={this.handleSelectAllClick}
                  onRequestSort={this.handleRequestSort}
                  rowCount={data.length}
                />
                <TableBody>
                  {data
                    .sort(getSorting(order, orderBy))
                    .slice(page * rowsPerPage, (page * rowsPerPage) + rowsPerPage)
                    .map((d) => {
                      const isSelected = this.isSelected(d.id);
                      return (
                        <TableRow
                          className="material-table__row"
                          role="checkbox"
                          onClick={event => this.handleClick(event, d.id)}
                          aria-checked={isSelected}
                          tabIndex={-1}
                          key={d.id}
                          selected={isSelected}
                        >
                          {/* <TableCell className="material-table__cell" padding="checkbox">
                            <Checkbox checked={isSelected} className="material-table__checkbox" />
                          </TableCell> */}
                          <TableCell
                            className="material-table__cell material-table__cell-right"
                            component="th"
                            scope="row"
                            padding="none"
                          >
                            {d.name}
                          </TableCell>
                          <TableCell
                            className="material-table__cell material-table__cell-right"
                          >{d.deviceID}
                          </TableCell>
                          <TableCell
                            className="material-table__cell material-table__cell-right"
                          >{d.status}
                          </TableCell>
                          <TableCell
                            className="material-table__cell material-table__cell-right"
                          >{d.actions}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 49 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </div>
            <TablePagination
              component="div"
              className="material-table__pagination"
              count={data.length}
              rowsPerPage={rowsPerPage}
              page={page}
              backIconButtonProps={{ 'aria-label': 'Previous Page' }}
              nextIconButtonProps={{ 'aria-label': 'Next Page' }}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
              rowsPerPageOptions={[5, 10, 15]}
              dir="ltr"
              SelectProps={{
                inputProps: { 'aria-label': 'rows per page' },
                native: true,
              }}
            />
          </CardBody>
        </Card>
        {this.state.alert}
        <Dialog
          open={this.state.openDatePicker}>

        </Dialog>
      </Col>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userInfo: state.userInfo
  }
}
export default reduxForm({
  form: 'form' // a unique identifier for this form
})(connect(mapStateToProps, null)(withTranslation('common')(MatTable)));