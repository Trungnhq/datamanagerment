import React from 'react';
import { connect } from 'react-redux';
import {
  AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer,
} from 'recharts';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import Panel from '../../../../shared/components/Panel';

import getTooltipStyles from '../../../../shared/helpers';

const data = [{ name: '01/11', a: 21, b: 60 },
  { name: '02/11', a: 23, b: 75 },
  { name: '03/11', a: 23, b: 80 },
  { name: '04/11', a: 25, b: 78 },
  { name: '05/11', a: 32, b: 71 },
  { name: '06/11', a: 30, b: 65 },
  { name: '07/11', a: 28, b: 67 }];

const ABTestingAnalytics = ({ t, dir, themeName }) => (
  <Panel md={12} lg={12} xl={12} title={"Nhiệt độ và độ ẩm"}>
    <div dir="ltr">
      <ResponsiveContainer height={250} className="dashboard__area">
        <AreaChart data={data} margin={{ top: 20, left: -15, bottom: 20 }}>
          <XAxis dataKey="name" tickLine={false} reversed={dir === 'rtl'} />
          <YAxis tickLine={false} orientation={dir === 'rtl' ? 'right' : 'left'} />
          <Tooltip {...getTooltipStyles(themeName, 'defaultItems')} />
          <Legend />
          <CartesianGrid />
          <Area name="Nhiệt độ" type="monotone" dataKey="a" fill="#ff3f34" stroke="#ff3f34" fillOpacity={0.2} />
          <Area name="Độ ẩm" type="monotone" dataKey="b" fill="#70bbfd" stroke="#70bbfd" fillOpacity={0.2} />
        </AreaChart>
      </ResponsiveContainer>
    </div>
  </Panel>
);

ABTestingAnalytics.propTypes = {
  t: PropTypes.func.isRequired,
  dir: PropTypes.string.isRequired,
  themeName: PropTypes.string.isRequired,
};

export default connect(state => ({ themeName: state.theme.className }))(withTranslation('common')(ABTestingAnalytics));
