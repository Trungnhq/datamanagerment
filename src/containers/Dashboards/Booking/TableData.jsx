/* eslint-disable react/no-array-index-key,react/no-typos */
import React from 'react';
import {
  Area, AreaChart, ResponsiveContainer, Tooltip,
} from 'recharts';
import {
  DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown, Table,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import DotsHorizontalIcon from 'mdi-react/DotsHorizontalIcon';
import * as firebase from 'firebase/app';
import Panel from 'shared/components/Panel';
import moment from 'moment';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import web3 from "config/web3"
import SweetAlert from 'react-bootstrap-sweetalert';
import IMG_LOAD from "shared/img/process.gif"
import accessControl from "config/accessControl"
const EthereumjsTx = require('ethereumjs-tx').Transaction;
const CustomTooltip = ({ active, payload }) => {
  if (active) {
    return (
      <div className="dashboard__total-tooltip">
        <p className="label">{`$${payload[0].value}`}</p>
      </div>
    );
  }

  return null;
};

CustomTooltip.propTypes = {
  active: PropTypes.bool,
  payload: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number,
  })),
};

CustomTooltip.defaultProps = {
  active: false,
  payload: null,
};

const DropDownMore = ({ index, handleConfirm, handleBanAccess }) => (
  <UncontrolledDropdown className="dashboard__table-more">
    <DropdownToggle>
      <p><DotsHorizontalIcon /></p>
    </DropdownToggle>
    <DropdownMenu className="dropdown__menu">
      <DropdownItem onClick={handleConfirm}>Cho phép</DropdownItem>
      <DropdownItem onClick={handleBanAccess}>Chặn</DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>
);

DropDownMore.propTypes = {
  index: PropTypes.number.isRequired,
  handleDeleteRow: PropTypes.func.isRequired,
};

class TopTen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      cryptoTable: [],
      alert: null
    }
    this.hideAlert = this.hideAlert.bind(this)
    this.listenAllEventRequestSuccess = this.listenAllEventRequestSuccess.bind(this)
  }
  componentDidMount() {
    this.listenAllEventRequestSuccess();
  }

  hideAlert() {
    this.setState({
      alert: null
    })
  }
  listenAllEventRequestSuccess() {
    const address = this.props.userInfo.key.address;
    accessControl.getPastEvents('ConfirmAccessDataSuccess', {
      // filter: { owner: address},
      fromBlock: 0,
      toBlock: 'latest'
    }, (error, events) => {
      console.log("Check", events);
      let result = [];
      for (let i = 0; i < events.length; i++) {
        console.log(events[i])
        if (events[i]) {
          if (events[i].returnValues.owner.toLowerCase() === address) {
            const email = web3.utils.toUtf8(events[i].returnValues.emailRequester) + " - " + events[i].returnValues.requester
            result.push({
              emailRequester: email,
              deviceId: events[i].returnValues.deviceId,
              requester: events[i].returnValues.requester,
              time: parseInt(events[i].returnValues.time),
            })
          }
        }
      }
      this.setState({ cryptoTable: result })
    })
      .then((events) => {
        // console.log(events) // same results as the optional callback above
      });
  }
  render() {
    return (
      <Panel lg={12} title={this.props.t("dataList.approvedList")}>
        <Table responsive className="table--bordered dashboard__table-crypto">
          <thead>
            <tr>
              <th>#</th>
              <th>{this.props.t("dataList.deviceId")}</th>
              <th>{this.props.t("dataList.requester")}</th>
              {/* <th>Thời gian</th>
          <th>Hành động</th> */}
              <th />
            </tr>
          </thead>
          <tbody>
            {this.state.cryptoTable.map((crypto, index) => (
              <tr key={index}>
                <td>{index + 1}</td>
                <td>{crypto.deviceId}</td>
                <td dir="ltr">{crypto.requester}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        {this.state.alert}
      </Panel>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userInfo: state.userInfo,
    newOrder: state.newOrder.items,
    rtl: state.rtl,
  }
}
export default connect(mapStateToProps, null)(withTranslation('common')(TopTen));
