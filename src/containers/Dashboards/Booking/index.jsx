import React, { Component } from 'react';
import { Col, Container, Row } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import TotalProfitEarned from './components/TotalProfitEarned';
import TotalCustomers from './components/TotalCustomers';
import TotalBookings from './components/TotalBookings';
import BookingCancels from './components/BookingCancels';
import Reservations from './components/Reservations';
import WeeklyStat from './components/WeeklyStat';
import Occupancy from './components/Occupancy';
import { RTLProps } from '../../../shared/prop-types/ReducerProps';
import TableData from "./TableData"
class BookingDashboard extends Component {
  constructor(props){
    super(props);
    this.state ={
      listData :[]
    }
  }

  render() {
    const { t, rtl } = this.props;

    return (
      <Container className="dashboard">
        <Row>
          <Col md={12}>
            {/* <h3 className="page-title">{t('dashboard_booking.page_title')}</h3> */}
            <h3 className="page-title">{t('dataList.approvedList')}</h3> 
          </Col>
        </Row>
      
        <Row>
         <TableData cryptoTable={this.state.listData}  />
        </Row>
      </Container>
    );
  }
}

export default connect(state => ({
  rtl: state.rtl,
}))(withTranslation('common')(BookingDashboard));
