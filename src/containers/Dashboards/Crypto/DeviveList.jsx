import React, { Component } from 'react';
import { Col, Container, Row } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import TopTen from './components/TopTen';
import { deleteCryptoTableData } from '../../../redux/actions/cryptoTableActions';
import { Button } from 'reactstrap';
import SweetAlert from 'react-bootstrap-sweetalert';
import { Field, reduxForm } from 'redux-form';
import accessControl from "../../../config/accessControl"
import web3 from "../../../config/web3"
import IMG_LOAD from "../../../shared/img/process.gif"
import ReactTable from "react-table";
import "../../../scss/react-table.css"
const EthereumjsTx = require('ethereumjs-tx').Transaction;
class CryptoDashboard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      alert: null,
      listDevice: []
    }
    this.handleNewDevice = this.handleNewDevice.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.getUsers = this.getUsers.bind(this);
    this.registerDevice = this.registerDevice.bind(this);
  }
  async componentDidMount() {
    await this.getUsers();
    //await this.filterList();
  }
  async convertString() {
    var x = await web3.utils.fromAscii("0x12345");
    console.log(x);
    var str = await web3.utils.toAscii(x);
    console.log(str);
  }
  handleChange = name => evt => {
    this.setState({
      [name]: evt.target.value
    })
  }
  async filterList() {
    const temp = this.state.ownerList;
    let list = [];
    for (let i = 0; i < temp.length; i++) {
      if (temp[i].address.toLowerCase() === this.props.userInfo.key.address.toLowerCase()) {
        var listdevice = temp[i].deviceList;
        for (let j = 0; j < listdevice.length; j++) {
          await list.push(
            {
              name: this.props.userInfo.username,
              deviceID: listdevice[j][0],
              status: this.props.t("dataList.active")
            }
          )
        }
      }
    }
    this.setState({
      listDevice: list
    })
  }
  registerDevice = async (deviceID, email) => {
    const privateKey = this.props.userInfo.key.privateKey;
    const address = this.props.userInfo.key.address;
    console.log("privateKey", privateKey)
    console.log("address", address)
    const register = await accessControl.methods.registerDevice(deviceID, email).encodeABI();
    let nonce = await web3.eth.getTransactionCount(address);
    console.log("nonce", nonce)
    console.log("contract address", accessControl.address)
    const txParams = {
      nonce: nonce,
      gasPrice: web3.utils.toHex(web3.utils.toWei('50', 'gwei')),
      gasLimit: 4000000,
      to: "0x739a933e6eb8ba6824767ae691289b8ed3348d46",
      data: register,
    }
    console.log("txParams", txParams)
    let tx = new EthereumjsTx(txParams, { chain: 'ropsten', hardfork: 'petersburg' });
    let sign = await Buffer.from(privateKey.slice(2), 'hex');
    tx.sign(sign);
    const rawTx = '0x' + tx.serialize().toString('hex');
    web3.eth.sendSignedTransaction(rawTx,
      (error, result) => {
        if (error) {
          console.log(error);
          this.setState({
            alert: (
              <SweetAlert
                error
                style={{ display: "block" }}
                title={this.props.t("dataList.failed")}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
              >
                Đăng ký thất bại
                  </SweetAlert>
            )
          });
        }
        else {
          console.log("Result", result);
          this.setState({
            alert: (
              <SweetAlert
                success
                style={{ display: "block" }}
                title={this.props.t("dataList.success")}
                onConfirm={() => this.hideAlert()}
                onCancel={() => this.hideAlert()}
                showCancel={false}
                confirmBtnText={this.props.t("dataList.cancle")}
                closeOnClickOutside={false}
              >
               {this.props.t("dataList.successRegister")}
                    </SweetAlert>
            )
          });
        }
      })
  }
  getUsers = async () => {
    const ownerList = await accessControl.methods.getUsers().call();
    this.setState({
      ownerList: [],
    })
    await ownerList.map(async (address) => {
      const deviceList = await accessControl.methods.getUserDevices(address).call();
      console.log(deviceList);
      await this.setState(prev => ({
        ownerList: [...prev.ownerList, { address, deviceList }]
      }))
      this.filterList();
    })

  }
  async handleSubmit() {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block" }}
          title={this.props.t("dataList.processing")}
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          showConfirm={false}
        >
          <div style={{ textAlign: "center" }}>
            <img
              alt="{name}"
              src={IMG_LOAD}
              style={{ height: "100px", width: "100px" }}
            />
          </div>
        </SweetAlert>
      )
    });
    const email = await web3.utils.fromAscii(this.props.userInfo.username);
    const deviceID = await web3.utils.fromAscii(this.state.deviceID);
    this.registerDevice(deviceID, email);

  }
  onDeleteCryptoTableData = (index, e) => {
    const { dispatch, cryptoTable } = this.props;
    e.preventDefault();
    const arrayCopy = [...cryptoTable];
    arrayCopy.splice(index, 1);
    dispatch(deleteCryptoTableData(arrayCopy));
  };
  hideAlert() {
    this.setState({
      alert: null
    })
  }
  handleNewDevice() {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block" }}
          title={this.props.t("dataList.enterDeviceId")}
          onConfirm={() => this.handleSubmit()}
          onCancel={() => this.hideAlert()}
          showConfirm={true}
          confirmBtnText={this.props.t("dataList.register")}
        >
          <form className="form" onSubmit={this.handleSubmit}>
            <div className="form__form-group">
              <span className="form__form-group-label">{this.props.t("dataList.deviceId")}</span>
              <div className="form__form-group-field">
                <Field
                  name="deviceID"
                  component="input"
                  type="text"
                  placeholder="0xabcd..."
                  required
                  onChange={this.handleChange("deviceID")}
                />
              </div>
            </div>
          </form>

        </SweetAlert>
      )
    });
  }
  render() {
    const {
      cryptoTable, t
    } = this.props;

    return (
      <Container className="dashboard">
        <Row>
          <Col md={12} style={{ display: "flex", flexFlow: "row" }}>
            <h3 className="page-title">{t('devicelist.deviceList')}</h3>
    <Button color="info" style={{ marginLeft: "20px" }} onClick={this.handleNewDevice}>{this.props.t('devicelist.addDevice')}</Button>
          </Col>
        </Row>
        <Row>
          <TopTen cryptoTable={this.state.listDevice} onDeleteCryptoTableData={this.onDeleteCryptoTableData} />
        </Row>
        {this.state.alert}
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userInfo: state.userInfo,
    cryptoTable: state.cryptoTable.items,
    rtl: state.rtl,
    theme: state.theme,
  }
}
export default reduxForm({ form: "device" })(connect(mapStateToProps, null)(withTranslation('common')(CryptoDashboard)));
