/* eslint-disable react/no-array-index-key,react/no-typos */
import React from 'react';
import {
  Area, AreaChart, ResponsiveContainer, Tooltip,
} from 'recharts';
import {
  DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown, Table,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import DotsHorizontalIcon from 'mdi-react/DotsHorizontalIcon';
import { CryptoTableProps } from '../../../../shared/prop-types/TablesProps';
import Panel from '../../../../shared/components/Panel';



const CustomTooltip = ({ active, payload }) => {
  if (active) {
    return (
      <div className="dashboard__total-tooltip">
        <p className="label">{`$${payload[0].value}`}</p>
      </div>
    );
  }

  return null;
};

CustomTooltip.propTypes = {
  active: PropTypes.bool,
  payload: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number,
  })),
};

CustomTooltip.defaultProps = {
  active: false,
  payload: null,
};

const DropDownMore = ({ index, handleDeleteRow }) => (
  <UncontrolledDropdown className="dashboard__table-more">
    <DropdownToggle>
      <p><DotsHorizontalIcon /></p>
    </DropdownToggle>
    <DropdownMenu className="dropdown__menu">
      <Link to={`/dashboard_crypto/edit/${index}`}><DropdownItem>Edit</DropdownItem></Link>
      <DropdownItem onClick={handleDeleteRow}>Delete</DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>
);

DropDownMore.propTypes = {
  index: PropTypes.number.isRequired,
  handleDeleteRow: PropTypes.func.isRequired,
};

const TopTen = ({ cryptoTable, onDeleteCryptoTableData, t }) => (
  // <Panel lg={12} title={t('dashboard_crypto.top_cryptocurrencies')}>
  <Panel lg={12} title={t('devicelist.deviceList')}>
    <Table responsive className="table--bordered dashboard__table-crypto">
      <thead>
        <tr>
          <th>#</th>
          <th>{t('devicelist.owner')}</th>
          <th>{t('devicelist.deviceId')}</th>
          <th>{t('devicelist.status')}</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {cryptoTable.map((crypto, index) => (
          <tr key={index}>
            <td>{index + 1}</td>
            <td>{crypto.name}</td>
            <td dir="ltr">{crypto.deviceID}</td>
            <td>{crypto.status}</td>
            <td>
              <DropDownMore index={index} handleDeleteRow={e => onDeleteCryptoTableData(index, e)} />
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  </Panel>
);

TopTen.propTypes = {
  cryptoTable: CryptoTableProps.isRequired,
  onDeleteCryptoTableData: PropTypes.func.isRequired,
  t: PropTypes.func.isRequired,
};

export default withTranslation('common')(TopTen);
