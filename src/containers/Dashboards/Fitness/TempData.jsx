import React from 'react';
import { Badge, Table } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import Panel from '../../../shared/components/Panel';

const RecentOrders = ({ t }) => (
  <Panel lg={12} title={"Nhiệt độ"}>
    <Table responsive className="table--bordered">
      <thead>
        <tr>
          <th>#</th>
          <th>Id thiết bị</th>
          <th>Chủ sở hữu</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>1</td>
          <td>In-123356</td>
          <td>Lok Morisson</td>
         
        </tr>
        <tr>
          <td>2</td>
          <td>In-254875</td>
          <td>Norman Brown</td>
          
        </tr>
        <tr>
          <td>3</td>
          <td>In-877868</td>
          <td>Sam Medinberg</td>
         
        </tr>
        <tr>
          <td>4</td>
          <td>In-619876</td>
          <td>Dave Morisson</td>
          
        </tr>
        <tr>
          <td>5</td>
          <td>In-218778</td>
          <td>Klara Brown</td>
         
        </tr>
        <tr>
          <td>6</td>
          <td>In-626268</td>
          <td>Molly Medinberg</td>
         
        </tr>
      </tbody>
    </Table>
  </Panel>
);

RecentOrders.propTypes = {
  t: PropTypes.func.isRequired,
};

export default withTranslation('common')(RecentOrders);
