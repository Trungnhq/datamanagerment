import React, { Component } from 'react';
import { Col, Container, Row } from 'reactstrap';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import { compose } from 'redux';
// import ActivityChart from './components/ActivityChart';
// import ActivityRating from './components/ActivityRating';
// import FatBurning from './components/FatBurning';
// import HeartRate from './components/HeartRate';
// import CaloriesBurn from './components/CaloriesBurn';
// import Steps from './components/Steps';
// import Distance from './components/Distance';
// import TodayRunningMap from './components/TodayRunningMap';
// import MyCompetitors from './components/MyCompetitors';
import { RTLProps } from '../../../shared/prop-types/ReducerProps';
import ReactTable from "react-table";
import 'react-table/react-table.css'
import TempData from "./TempData"
import HumidData from "./HumidData"
import MatTable from "../../Tables/MaterialTable/components/MatTable"
import MatTableHumid from "../../Tables/MaterialTable/components/MatTableHumid"
class FitnessDashboard extends Component {
  constructor(props){
    super(props)
    this.state={
      data: []
    }
  }
  render(){
    const {t} = this.props;
    return(
      <Container className="dashboard">
      <Row>
        <Col md={12}>
          {/* <h3 className="page-title">{t('dashboard_fitness.page_title')}</h3> */}
    <h3 className="page-title">{t('dashboard_fitness.page_title')}</h3>
        </Col>
      </Row>
      {/* <Row>
        <HeartRate />
        <CaloriesBurn />
        <Steps />
        <Distance />
      </Row> */}
      {/* <Row>
        <ActivityChart dir={rtl.direction} />
        <TodayRunningMap />
        <MyCompetitors />
        <FatBurning dir={rtl.direction} />
        <ActivityRating dir={rtl.direction} />
      </Row> */}
      <Row>
        <MatTable name={t('dataList.page_title')}/>
      </Row>
      {/* <Row>
      <MatTableHumid name={"Độ ẩm"}/>
      </Row> */}
    </Container>
    )
  }
  }
FitnessDashboard.propTypes = {
  t: PropTypes.func.isRequired,
  rtl: RTLProps.isRequired,
};

export default compose(withTranslation('common'), connect(state => ({
  rtl: state.rtl,
})))(FitnessDashboard);
