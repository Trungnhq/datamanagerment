import React, { Component } from 'react';
import { Col, Container, Row } from 'reactstrap';
import { withTranslation } from 'react-i18next';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import TotalProducts from './components/TotalProducts';
import TotalProfit from './components/TotalProfit';
import OrdersToday from './components/OrdersToday';
import Subscriptions from './components/Subscriptions';
import TopSellingProducts from './components/TopSellingProducts';
import BasicCard from './components/BasicCard';
import SalesStatistic from './components/SalesStatistic';
import RecentOrders from './components/tableList.jsx';
import ProductSales from './components/ProductSales';
import NewOrders from './components/NewOrders';
import SalesStatistisBar from './components/SalesStatistisBar';
import MyTodos from './components/MyTodos';
import Emails from './components/Emails';
import SalesReport from './components/SalesReport';
import ShortReminders from './components/ShortReminders';
import { deleteNewOrderTableData } from '../../../redux/actions/newOrderTableActions';
import { NewOrderTableProps } from '../../../shared/prop-types/TablesProps';
import { RTLProps } from '../../../shared/prop-types/ReducerProps';
import accessControl from "config/accessControl"
class ECommerceDashboard extends Component {
constructor(props){
  super(props);
  this.state= {
    listData: []
  }
  this.listenAllEventRequestSuccess = this.listenAllEventRequestSuccess.bind(this);
  this.listenAllEventConfirmSuccess = this.listenAllEventConfirmSuccess.bind(this);

}
  async componentDidMount(){
    await this.listenAllEventConfirmSuccess();
    await this.listenAllEventRequestSuccess();
  }
  onDeleteRow = (index, e) => {
    const { dispatch, newOrder } = this.props;

    e.preventDefault();
    const arrayCopy = [...newOrder];
    arrayCopy.splice(index, 1);
    dispatch(deleteNewOrderTableData(arrayCopy));
  };

  listenAllEventRequestSuccess() {
    const address = this.props.userInfo.key.address;
    accessControl.getPastEvents('RequestDataSuccess', {
        filter: { owner: address},
        fromBlock: 0,
        toBlock: 'latest'
    }, (error, events) => {
        console.log("Check", events);
        let result = [];
        for(let i = 0;i <events.length;i++){
          var check = false;
        
          if(events[i]){
            //for(var z = 0;z<this.state.successList.length;z++){
            //   if(this.state.successList[z].owner === events[i].returnValues.owner){
            //     check = true
            //   }
            // }
            // if(check === false){
              result.push({
                requester: events[i].returnValues.requester,
                time: events[i].returnValues.time,
                deviceId: events[i].returnValues.deviceId,
                emailRequester:  events[i].returnValues.emailRequester
              })
          //  }
          
          }
        }
        console.log("result", result)
        this.setState({ listData: result })
    })
        .then((events) => {
           // console.log(events) // same results as the optional callback above
        });
}
listenAllEventConfirmSuccess() {
  const address = this.props.userInfo.key.address;
  accessControl.getPastEvents('ConfirmAccessDataSuccess', {
    filter: { requester: address },
    fromBlock: 0,
    toBlock: 'latest'
  }, (error, events) => {
    console.log("Check event", events);
    let result = [];
    for (let i = 0; i < events.length; i++) {
      console.log(events[i])
      if (events[i]) {
        result.push({
          requester: events[i].returnValues.requester,
          time: events[i].returnValues.time,
          deviceId: events[i].returnValues.deviceId,
          emailRequester: events[i].returnValues.emailRequester,
          owner: events[i].returnValues.owner
        })
      }
    }
    this.setState({ successList: result })
  })
    .then((events) => {
      // console.log(events) // same results as the optional callback above
    });
}
  render() {
    const { t, newOrder, rtl } = this.props;

    return (
      <Container className="dashboard">
        <Row>
          <Col md={12}>
            {/* <h3 className="page-title">{t('dashboard_commerce.page_title')}</h3> */}
            <h3 className="page-title">{t('dataList.requestList')}</h3>
          </Col>
        </Row>
        {/* <Row>
          <TotalProducts />
          <TotalProfit />
          <OrdersToday />
          <Subscriptions />
        </Row> */}
        <Row>
          {/* <ProductSales rtl={rtl.direction} />
          <BasicCard />
          <SalesStatistic />
          <MyTodos />
          <SalesStatistisBar />
          <SalesReport />
          <Emails />
          <ShortReminders />
          <TopSellingProducts dir={rtl.direction} />
          <NewOrders newOrder={newOrder} onDeleteRow={this.onDeleteRow} /> */}
          <RecentOrders cryptoTable={this.state.listData} />
        </Row>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userInfo: state.userInfo,
    newOrder: state.newOrder.items,
    rtl: state.rtl,
  }
}
export default connect(mapStateToProps, null)(withTranslation('common')(ECommerceDashboard));
