/* eslint-disable react/no-array-index-key,react/no-typos */
import React from 'react';
import {
  Area, AreaChart, ResponsiveContainer, Tooltip,
} from 'recharts';
import {
  DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown, Table,
} from 'reactstrap';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withTranslation } from 'react-i18next';
import DotsHorizontalIcon from 'mdi-react/DotsHorizontalIcon';
import * as firebase from 'firebase/app';
import Panel from '../../../../shared/components/Panel';
import moment from 'moment';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import web3 from "config/web3"
import SweetAlert from 'react-bootstrap-sweetalert';
import IMG_LOAD from "shared/img/process.gif"
import accessControl from "config/accessControl"
const EthereumjsTx = require('ethereumjs-tx').Transaction;
const CustomTooltip = ({ active, payload }) => {
  if (active) {
    return (
      <div className="dashboard__total-tooltip">
        <p className="label">{`$${payload[0].value}`}</p>
      </div>
    );
  }

  return null;
};

CustomTooltip.propTypes = {
  active: PropTypes.bool,
  payload: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.number,
  })),
};

CustomTooltip.defaultProps = {
  active: false,
  payload: null,
};

const DropDownMore = ({ index, handleConfirm, handleBanAccess }) => (
  <UncontrolledDropdown className="dashboard__table-more">
    <DropdownToggle>
      <p><DotsHorizontalIcon /></p>
    </DropdownToggle>
    <DropdownMenu className="dropdown__menu">
      <DropdownItem onClick={handleConfirm}>Cho phép</DropdownItem>
      <DropdownItem onClick={handleBanAccess}>Chặn</DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>
);

DropDownMore.propTypes = {
  index: PropTypes.number.isRequired,
  handleDeleteRow: PropTypes.func.isRequired,
};

class TopTen extends React.Component {
constructor(props){
  super(props);
  this.state = {
    cryptoTable: [],
    alert: null,
  }
  this.handleConfirmAccess = this.handleConfirmAccess.bind(this);
  this.hideAlert = this.hideAlert.bind(this);
}
componentDidMount(){
  if(this.props.cryptoTable.length > 0){
    this.setState({
      cryptoTable: this.props.cryptoTable
    })
  }
}
componentWillReceiveProps(preProps){
  if(preProps.cryptoTable !== this.props.cryptoTable){
    this.setState({
      cryptoTable: preProps.cryptoTable
    })
  }
}

hideAlert(){
  this.setState({
    alert: null
  })
}
async handleConfirmAccess(ownerAddress, deviceId, requester, emailRequester){
  this.setState({
    alert: (
      <SweetAlert
        warning
        style={{ display: "block" }}
        title="Đang xử lý!"
        onConfirm={() => this.hideAlert()}
        onCancel={() => this.hideAlert()}
        showConfirm={false}
      >
        <div style={{ textAlign: "center" }}>
          <img
            alt="{name}"
            src={IMG_LOAD}
            style={{ height: "100px", width: "100px" }}
          />
        </div>
      </SweetAlert>
    )
  });
  const email_Request = web3.utils.toUtf8(emailRequester);
  const username_Request = email_Request.substring(0, email_Request.lastIndexOf("@"));
  console.log(username_Request)
  var database = firebase.database()
  var TempRef = database.ref("reEncryptionKey");
  var TempRefChild = TempRef.child(`${deviceId}`);
  var Sub_TempRefChild = TempRefChild.child(`${username_Request}`)
  await Sub_TempRefChild.on("value", async (snapshot) => {
    var data = await snapshot.val();
    const reEn_key = web3.utils.fromAscii(data.key);
    const privateKey = this.props.userInfo.key.privateKey;
    const address = this.props.userInfo.key.address;
    console.log("privateKey", privateKey)
    console.log("address", address)
    const email_bytes_owner = web3.utils.fromAscii(this.props.userInfo.username);
    const register = await accessControl.methods.confirmAccessData(
      this.props.userInfo.key.address,
      email_bytes_owner,
      deviceId,
      requester,
      emailRequester,
      reEn_key
      ).encodeABI();
    let nonce = await web3.eth.getTransactionCount(address);
    console.log("nonce", nonce)
    console.log("contract address", accessControl.address)
    const txParams = {
        nonce: nonce,
        gasPrice: web3.utils.toHex(web3.utils.toWei('50', 'gwei')),
        gasLimit: 4000000,
        to: "0x739a933e6eb8ba6824767ae691289b8ed3348d46",
        data: register,
    }
    console.log("txParams", txParams)
    let tx = new EthereumjsTx(txParams, {chain:'ropsten', hardfork: 'petersburg'});
    let sign = await Buffer.from(privateKey.slice(2), 'hex');
    tx.sign(sign);
    const rawTx = '0x' + tx.serialize().toString('hex');
    web3.eth.sendSignedTransaction(rawTx,
        (error, result) => {
            if (error) { 
              console.log(error) ;
              this.setState({
                alert: (
                  <SweetAlert
                    error
                    style={{ display: "block"}}
                    title={this.props.t("dataList.failed")}
                    onConfirm={() => this.hideAlert()}
                    onCancel={() => this.hideAlert()}
                  >
                   Đăng ký thất bại
                  </SweetAlert>
                )
              });}
            else {
                console.log("Result", result);
                this.setState({
                  alert: (
                    <SweetAlert
                      success
                      style={{ display: "block" }}
                      title={this.props.t("dataList.success")}
                      onConfirm={() => this.hideAlert()}
                      onCancel={() => this.hideAlert()}
                      showCancel={false}
                      confirmBtnText={this.props.t("dataList.cancle")}
                      closeOnClickOutside={false}
                    >
                      {this.props.t("dataList.successRegister")}
                    </SweetAlert>
                  )
                });
            }
        })
    }, function (errorObject) {
      console.log("The read failed: " + errorObject.code);
    });
}
render(){
  return(
    <Panel lg={12} title={this.props.t("dataList.requestList")}>
    <Table responsive className="table--bordered dashboard__table-crypto">
      <thead>
        <tr>
          <th>#</th>
  <th>{this.props.t("dataList.deviceId")}</th>
          <th>{this.props.t("dataList.requester")}</th>
          <th>{this.props.t("devicelist.time")}</th>
          <th>{this.props.t("dataList.actions")}</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {this.state.cryptoTable.map((crypto, index) => (
          <tr key={index}>
            <td>{index + 1}</td>
            <td>{crypto.deviceId}</td>
            <td dir="ltr">{crypto.requester}</td>
            <td>{moment(parseInt(crypto.time * 1000)).format("DD/MM/YYYY hh:ss")}</td>
            <td>
              <Button 
              color="info"
              size="sm"
              //handleBanAccess={() => this.handleBanAccess()}
              onClick={() => this.handleConfirmAccess(this.props.userInfo.key.address,crypto.deviceId,crypto.requester, crypto.emailRequester)} >{this.props.t("dataList.accept")}</Button>
              
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
    {this.state.alert}
    
  </Panel>
  )
}
} 

const mapStateToProps = (state) => {
  return {
    userInfo: state.userInfo,
    newOrder: state.newOrder.items,
    rtl: state.rtl,
  }
}
export default  connect(mapStateToProps, null)(withTranslation('common')(TopTen))
