import React, { Component } from 'react';
import DownIcon from 'mdi-react/ChevronDownIcon';
import { Collapse } from 'reactstrap';
import TopbarMenuLink from './TopbarMenuLink';
import { UserProps, AuthOProps } from '../../../shared/prop-types/ReducerProps';
import { hookAuth0 } from '../../../shared/components/auth/withAuth0';
import { connect } from 'react-redux';
const Ava = `${process.env.PUBLIC_URL}/img/ava.png`;

class TopbarProfile extends Component {
  static propTypes = {
    user: UserProps.isRequired,
    auth0: AuthOProps.isRequired,
  }


  constructor(props) {
    super(props);
    this.state = {
      collapse: false,
    };
  }
  componentDidMount(){
    if(!this.props.userInfo.data.user){
      this.props.cleanStore()
    }
  }
  toggle = () => {
    this.setState(prevState => ({ collapse: !prevState.collapse }));
  };

  logout = () => {
    this.props.cleanStore();
  }

  render() {
    const { user, auth0 } = this.props;
    const { collapse } = this.state;

    return (
      <div className="topbar__profile">
        <button className="topbar__avatar" type="button" onClick={this.toggle}>
          <img
            className="topbar__avatar-img"
            src={(auth0.user && auth0.user.picture) || user.avatar || Ava}
            alt="avatar"
          />
          <p className="topbar__avatar-name">
            {/* { auth0.loading ? 'Loading...' : (auth0.user && auth0.user.name) || user.fullName} */}
            { auth0.loading ? 'Loading...' : this.props.userInfo.data.user ? this.props.userInfo.data.user.email : ""}
          </p>
          <DownIcon className="topbar__icon" />
        </button>
        {collapse && <button className="topbar__back" type="button" onClick={this.toggle} />}
        <Collapse isOpen={collapse} className="topbar__menu-wrap">
          <div className="topbar__menu">
            <TopbarMenuLink
              title="My Profile"
              icon="user"
              path="/account/profile"
              onClick={this.toggle}
            />
            <TopbarMenuLink
              title="Calendar"
              icon="calendar-full"
              path="/default_pages/calendar"
              onClick={this.toggle}
            />
            <TopbarMenuLink
              title="Tasks"
              icon="list"
              path="/todo"
              onClick={this.toggle}
            />
            <TopbarMenuLink
              title="Inbox"
              icon="inbox"
              path="/mail"
              onClick={this.toggle}
            />
            <div className="topbar__menu-divider" />
            <TopbarMenuLink
              title="Account Settings"
              icon="cog"
              path="/account/profile"
              onClick={this.toggle}
            />
            <TopbarMenuLink
              title="Lock Screen"
              icon="lock"
              path="/lock_screen"
              onClick={this.toggle}
            />
            {auth0.isAuthenticated && (
              <TopbarMenuLink
                title="Log Out Auth0"
                icon="exit"
                path="/log_in"
                onClick={auth0.logout}
              />
            )
            }
            <TopbarMenuLink
              title="Log Out"
              icon="exit"
              path="/log_in"
              onClick={this.logout}
            />
          </div>
        </Collapse>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userInfo: state.userInfo,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    cleanStore: () => dispatch({ type: "CLEAN_STORE"}),
  }
}
export default hookAuth0(connect(mapStateToProps, mapDispatchToProps)(TopbarProfile));
