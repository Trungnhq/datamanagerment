import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import EyeIcon from 'mdi-react/EyeIcon';
import KeyVariantIcon from 'mdi-react/KeyVariantIcon';
import AccountOutlineIcon from 'mdi-react/AccountOutlineIcon';
import MailRuIcon from 'mdi-react/MailRuIcon';
import { Button, Alert } from 'reactstrap';
import PropTypes from 'prop-types';
import ethereumjsWallet from 'ethereumjs-wallet';

class RegisterForm extends Component {

  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    errorMessage: PropTypes.string,
  };

  static defaultProps = {
    errorMessage: '',
  };

  constructor(props) {
    super(props);
    this.state = {
      showPassword: false,
    };

    this.showPassword = this.showPassword.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.initWallet = this.initWallet.bind(this)
  }
  componentDidMount(){
    //this.initWallet()
  }
  initWallet(){
    const account = ethereumjsWallet.generate();
    const privateKey = account.getPrivateKeyString();
    const publicKey = account.getPublicKeyString();
    const address = account.getAddressString();
    const data = {
      privateKey: privateKey,
      publicKey: publicKey,
      address: address
    }
    console.log(privateKey);
  }
  showPassword(e) {
    e.preventDefault();
    this.setState(prevState => ({ showPassword: !prevState.showPassword }));
  }
  handleChange = name => evt => {
    this.setState({
      [name]: evt.target.value
    })
  }
  render() {
    const { handleSubmit, errorMessage } = this.props;
    const { showPassword } = this.state;

    return (
      <form className="form" onSubmit={handleSubmit}>
        <Alert
          color="danger"
          isOpen={!!errorMessage}
        >
          {errorMessage}
        </Alert>
        {/* <div className="form__form-group">
          <span className="form__form-group-label">Tên đăng nhập</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <AccountOutlineIcon />
            </div>
            <Field
              name="username"
              component="input"
              type="text"
              placeholder="Name"
            />
          </div>
        </div> */}
        <div className="form__form-group">
          <span className="form__form-group-label">E-mail</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <MailRuIcon />
            </div>
            <Field
              name="email"
              component="input"
              type="email"
              placeholder="example@mail.com"
              required
            />
          </div>
        </div>
        <div className="form__form-group form__form-group--forgot">
          <span className="form__form-group-label">Mật khẩu</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <KeyVariantIcon />
            </div>
            <Field
              name="password"
              component="input"
              type={showPassword ? 'text' : 'password'}
              placeholder="Password"
              required
            />
            <button
              type="button"
              className={`form__form-group-button${showPassword ? ' active' : ''}`}
              onClick={e => this.showPassword(e)}
            ><EyeIcon />
            </button>
          </div>
        </div>
        <div className="account__btns register__btns">
          <Button type="submit" color="primary" className="account__btn">
            Đăng ký
          </Button>
        </div>
      </form>
    );
  }
}

export default reduxForm({
  form: 'register_form', // a unique identifier for this form
})(RegisterForm);
