import React, { Component } from 'react';
import { Field, reduxForm, Form } from 'redux-form';
import { connect } from 'react-redux';
import EyeIcon from 'mdi-react/EyeIcon';
import KeyVariantIcon from 'mdi-react/KeyVariantIcon';
import AccountOutlineIcon from 'mdi-react/AccountOutlineIcon';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Alert, Button } from 'reactstrap';
import renderCheckBoxField from '../form/CheckBox';
import * as firebase from 'firebase/app';
import { withRouter } from 'react-router';
import SweetAlert from 'react-bootstrap-sweetalert';
import IMG_LOAD from "../../img/process.gif"
import { withTranslation } from 'react-i18next';
class LogInForm extends Component {
  static defaultProps = {
    errorMessage: '',
    errorMsg: '',
    fieldUser: 'Username',
    typeFieldUser: 'text',
  }

  constructor() {
    super();
    this.state = {
      showPassword: false,
      alert: null
    };

    this.showPassword = this.showPassword.bind(this);
    this.onSubmitFireBase = this.onSubmitFireBase.bind(this);
    this.handleChange = this.handleChange.bind(this);  
    this.hideAlert = this.hideAlert.bind(this);
    this.getDataFireBase = this.getDataFireBase.bind(this)
  }
  handleChange = name => evt => {
    this.setState({
      [name]: evt.target.value
    })
  }
  showPassword(e) {
    e.preventDefault();
    this.setState(prevState => ({ showPassword: !prevState.showPassword }));
  }
  hideAlert(){
    this.setState({
      alert: null
    })
  }
  componentDidMount(){
    console.log(this.props.userInfo.data)
    if(this.props.userInfo.key){
      this.props.history.push('/dashboard_mobile_app');
    }
  }
  async onSubmitFireBase () {
    this.setState({
      alert: (
        <SweetAlert
          warning
          style={{ display: "block" }}
          title={this.props.t("dataList.processing")}
          onConfirm={() => this.hideAlert()}
          onCancel={() => this.hideAlert()}
          showConfirm={false}
        >
          <div style={{ textAlign: "center" }}>
            <img
              alt="{name}"
              src={IMG_LOAD}
              style={{ height: "100px", width: "100px" }}
            />
          </div>
        </SweetAlert>
      )
    });
    this.setState({ error: '' });
    await firebase.auth().signInWithEmailAndPassword(this.state.username, this.state.pass).then(async (res) => {
      var name   = this.state.username.substring(0, this.state.username.lastIndexOf("@"));
      await this.setState({
        response: res
      })
      this.props.saveUserInfo(this.state.response, this.state.pass, this.state.username, "");
      await this.getDataFireBase(name);
      this.setState({alert: null})
      this.props.history.push('/dashboard_fitness');
    }).catch((error) => {
      this.setState({ error: error.message });
    });
  };
  async getDataFireBase(name){
    var database = firebase.database()
    var TempRef = database.ref("account");
    var TempRefChild = TempRef.child(`${name}`);
    await TempRefChild.on("value", async (snapshot) => {
      var data = await snapshot.val();
      console.log("Key", data)
      await this.setState({
        dataKey: data
      })
      this.props.saveUserInfo(this.state.response, this.state.pass, this.state.username, data);
      }, function (errorObject) {
        console.log("The read failed: " + errorObject.code);
      });
  }
  render() {
    const {
      handleSubmit, errorMessage, errorMsg, fieldUser, typeFieldUser, form,
    } = this.props;
    const { showPassword } = this.state;
    return (
      <Form className="form login-form" onSubmit={handleSubmit}>
        <Alert
          color="danger"
          isOpen={!!errorMessage || !!errorMsg}
        >
          {errorMessage}
          {errorMsg}
        </Alert>
        <div className="form__form-group">
          <span className="form__form-group-label">Email</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <AccountOutlineIcon />
            </div>
            <Field
              name="username"
              component="input"
              type={typeFieldUser}
              placeholder={fieldUser}
              onChange={this.handleChange("username")}
            />
          </div>
        </div>
        <div className="form__form-group">
          <span className="form__form-group-label">Mật khẩu</span>
          <div className="form__form-group-field">
            <div className="form__form-group-icon">
              <KeyVariantIcon />
            </div>
            <Field
              name="password"
              component="input"
              type={showPassword ? 'text' : 'password'}
              placeholder="Password"
              onChange={this.handleChange("pass")}
            />
            <button
              type="button"
              className={`form__form-group-button${showPassword ? ' active' : ''}`}
              onClick={e => this.showPassword(e)}
            ><EyeIcon />
            </button>
            <div className="account__forgot-password">
              <a href="/">Quên mật khẩu?</a>
            </div>
          </div>
        </div>
        <div className="form__form-group">
          <div className="form__form-group form__form-group-field">
            <Field
              name={`remember_me-${form}`}
              component={renderCheckBoxField}
              label="Ghi nhớ đăng nhập"
            />
          </div>
        </div>
        <div className="account__btns">
          <Button className="account__btn" submit="true" color="primary" onClick={() => this.onSubmitFireBase()}>Đăng nhập</Button>
          <Link className="btn btn-outline-primary account__btn" to="/register">
            Đăng ký
          </Link>
        </div>
        {this.state.alert}
      </Form>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    userInfo: state.userInfo,
    errorMsg: state.user.error,
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    saveUserInfo: (data, password, username, key) => dispatch({ type: "SAVE_ACCOUNT_INFO", data: data, password: password, username: username, dataKey: key }),
  }
}
export default withRouter(connect(mapStateToProps,mapDispatchToProps)(reduxForm()(withTranslation("common")(LogInForm))));
