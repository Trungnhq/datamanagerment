import themeReducer from './themeReducer';
import rtlReducer from './rtlReducer';
import sidebarReducer from './sidebarReducer';
import cryptoTableReducer from './cryptoTableReducer';
import newOrderTableReducer from './newOrderTableReducer';
import customizerReducer from './customizerReducer';
import todoReducer from './todoReducer';
import authReducer from './authReducer';
import userInfo from "./userInfo";
import walletInfo from "./wallet"
import { reducer as reduxFormReducer } from 'redux-form';
import { combineReducers } from 'redux';
import storage from 'redux-persist/lib/storage';
const appReducer = combineReducers({
  form: reduxFormReducer, // mounted under "form",
  theme: themeReducer,
  rtl: rtlReducer,
  sidebar: sidebarReducer,
  cryptoTable: cryptoTableReducer,
  newOrder: newOrderTableReducer,
  customizer: customizerReducer,
  todos: todoReducer,
  user: authReducer,
  userInfo,
  walletInfo
});


const rootReducer = (state, action) => {
  console.log(action.type)
  if (action.type === 'CLEAN_STORE') {
    state = undefined;
    storage.removeItem('persist:root')
  }
  return appReducer(state, action);
};

export default rootReducer;