import {  SAVE_WALLET, SAVE_WALLET_FAILED, SAVE_WALLET_SUCCESS } from "../actions/type";

const initialState = { data: "", password: "" };
const reducer = (state = initialState, action) => {
    switch (action.type) {
        case  SAVE_WALLET:
            return {
                ...state,
            };
        case SAVE_WALLET_SUCCESS:
            return {
                ...state,
                data: action.data,
            };
        case SAVE_WALLET_FAILED:
            return {
                ...state,
            };
        default:
            return state;
    }
}
export default reducer;