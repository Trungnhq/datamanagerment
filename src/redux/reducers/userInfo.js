import { SAVE_ACCOUNT_INFO, SAVE_ACCOUNT_INFO_SUCCESS, SAVE_ACCOUNT_INFO_FAILURE } from "../actions/type";

const initialState = { data: "", password: "" };
const reducer = (state = initialState, action) => {
    switch (action.type) {

        case SAVE_ACCOUNT_INFO:
            return {
                ...state,
            };
        case SAVE_ACCOUNT_INFO_SUCCESS:
            return {
                ...state,
                data: action.data,
                password: action.password,
                username: action.username,
                key: action.key
            };
        case SAVE_ACCOUNT_INFO_FAILURE:
            return {
                ...state,
            };
        default:
            return state;
    }
}
export default reducer;